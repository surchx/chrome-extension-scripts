#!/bin/bash -e

set -x

# don't need this anymore pretty sure
# az login --service-principal -u ${sp_username} -p ${sp_password} --tenant ${tenant}

export AZURE_STORAGE_CONNECTION_STRING="${AZURE_STORAGE_CONNECTION_STRING}"

export BLOB_ENV=$BITBUCKET_BRANCH
if [ "$BITBUCKET_BRANCH" = "master" ]; then 
  export BLOB_ENV=dev
  ITEM_ID=bglojdmmfpciifimnjianefbifpgcama
  LATEST=latest.zip
fi
if [ "$BITBUCKET_BRANCH" = "qa" ]; then 
  export BLOB_ENV=test
  ITEM_ID=bglojdmmfpciifimnjianefbifpgcama
  LATEST=latest.zip
fi
if [ "$BITBUCKET_BRANCH" = "production" ]; then 
  export BLOB_ENV=production
  ITEM_ID=bjljoghcnmjaidglifbbngnbjefgjghk
  LATEST=latest.zip
fi
echo work with container ${BLOB_ENV}
# az storage blob list -c ${BLOB_ENV}
cd hosted/
az storage blob download-batch -d ../dist-hosted/ -s ${BLOB_ENV} --pattern \*.js
ls -l ../dist-hosted/
for var in *.js; do ls ${var}; done
for var in *.js; do
  az storage blob list -c ${BLOB_ENV} --prefix ${var} --output table
  ls -l ${var}
  if [ -f ../dist-hosted/${var} ]; then
    echo file exists... diff ${var}
    #-     diff ${var} ../../dist-hosted/ > /dev/null 2>&1
    different=$(diff ${var} ../dist-hosted/ > /dev/null 2>&1 && echo "same" || echo "different")
    if [ "${different}" = "different" ]; then
      echo upload ${var}
      az storage blob upload --container-name ${BLOB_ENV} --file ${var} --name ${var}
    else
      echo file ${var} appears to be the same
    fi
  else
    echo file does not exist... upload ${var}
    az storage blob upload --container-name ${BLOB_ENV} --file ${var} --name ${var}
  fi
  # az storage blob download --container-name ${BLOB_ENV} --name ${var} --file ../../dist/${var} --output table
  echo "done with ${var}"
done
cd ../
