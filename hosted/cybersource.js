currenturl = window.location.href

console.log("********************************************************", window.bradwashere)
console.log("********************************************************", window.bradwashere)
console.log("********************************************************", window.parent.bradwashere)
console.log("********************************************************", window.parent.bradwashere)

try {
  console.log($) // function()
  console.log(typeof $) // "function"

  console.log($()) // null
  console.log(typeof $()) // "object"

  console.log($().jquery)
  console.log(typeof $().jquery)
} catch (e) {
  console.log(e)
} // TypeError: "$() is null"

// In jQuery, $() is an alias of jQuery():
try {
  console.log(jQuery)
  console.log(typeof jQuery)

  console.log(jQuery())
  console.log(typeof jQuery())

  console.log(jQuery().jquery)
  console.log(typeof jQuery().jquery)
} catch (e) {
  console.log(e)
}




if (window.jQuery) {
  // jQuery is loaded
  alert("Yeah!");
} else {
  // jQuery is not loaded
  alert("Doesn't Work");
}

let currentPage = undefined

console.log("initializing....")

let otp = $("span").filter(function() { return ($(this).text() === 'One-Time Payment') });
console.log("OTP", otp)

if (otp) {
  console.log("we are on the right page")
} else {
  console.log("could not determine currentPage")
}

let slash = "/"
let doubleSlash = slash + slash

let amountSelector = doubleSlash + "label[text()='Amount *']/..//input"
let cardNumberSelector = doubleSlash + "label[text()='Credit Card Number *']/..//input"
let zipSelector = doubleSlash + "label[text()='Zip/Postal Code *']/..//input"


// This code is used to replace the amount input with a new input that does not contain the onblur
// event which contained a function that would automatically change the total of
// the surcharge to 0 when the user unfocused it
replace_amount_input = () => {
  $(document.body).xpath(amountSelector).val("100.00")
}

clicked_to_set_clicked = () => {
  zip = $(document.body).xpath(zipSelector).val()
  creditcardnumber = $(document.body).xpath(cardNumberSelector).val()
  amount = $(document.body).xpath(amountSelector).val()
  checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
}

// This code is used to extract the transaction id to be posted to the capture api as mTxId
extract_mTxId = () => {
  var withWhitespace = $("b:contains('Transaction ID: ')").parent().text()
  var s = withWhitespace.split(":")
  var text = undefined
  if (s.length == 2) {
    text = s[1].replace(/\D/g, '')
  } else {
    console.log("could not parse transaction id", withWhitespace)
  }
  return text
}

// Remove saved stxid that is used to be posted to capture api
delete_old_stxid = () =>{
  if (typeof sessionStorage.getItem('stxid') != null && sales_page_confirmation == true && sales_page_transaction_receipt_page_confirmation == 1 )
{
    sessionStorage.removeItem('stxid')
}
}
// ---End Helper Functions---

// This code is used to change the amount when set amount is pressed.

$("#level3total").click(() => {
  setTimeout(clicked_to_set_clicked,450)
})

let click_to_set_amount = $("#level3total").parent()
$(click_to_set_amount).click(() => {
  setTimeout(clicked_to_set_clicked,450)
})
// Declare global variables
// sTxId and id_set is used to keep track of the current sTxId
var sTxId;
var id_set = false;
var creditcardnumber;
var nicn
var zip;
var amount;

// retreive endpoint and apikey from sessionStorage
var endpoint = sessionStorage.getItem('endpoint')
var apikey = sessionStorage.getItem('apikey')

// not necessary for cybersource???   replace_amount_input()
delete_old_stxid()

// detects change in input
if (sales_page_confirmation == true) {

  /*
   Sale "OK" div
  <div class="skin-bg-table-odd struct-paddingSpace3--all css-43flnd-CSS-CSS" align-items="center" data-test="section-replyFlag"><div class="struct-marginSpace5--right text-heading5 css-k20bs-CSS-CSS"><span>Result Code</span></div><div class="u-word-break-break-word css-5caqji-CSS-CSS">SOK - Request was processed successfully.</div></div>
   */

  /*
    Sale page div
    <div align-items="center" class="css-brusys-CSS-CSS"><h1 class="css-q9x4sg-all-structMargin-pageTitle-pageTitle">Transaction Summary</h1></div>
   */

  $(document.body).xpath(amountSelector).keyup((e) => {
    amount = e.target.value
    if (e.keyCode != 39 && e.keyCode != 37 && e.keyCode!= 17  && e.keyCode!= 18  && e.keyCode!= 17  && e.keyCode!= 38  && e.keyCode!= 40  && e.keyCode!= 9  && e.keyCode!= 13  && e.keyCode!= 16)
    {
      checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
    }
  })

  $(document.body).xpath(zipSelector).keyup((e) => {
    zip = e.target.value
    if (e.keyCode != 39 && e.keyCode != 37 && e.keyCode!= 17  && e.keyCode!= 18  && e.keyCode!= 17  && e.keyCode!= 38  && e.keyCode!= 40  && e.keyCode!= 9  && e.keyCode!= 13  && e.keyCode!= 16)
    {
      checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
    }
  })

  $(document.body).xpath(cardNumberSelector).keyup((e) => {
    creditcardnumber = e.target.value
    if (e.keyCode != 39 && e.keyCode != 37 && e.keyCode!= 17  && e.keyCode!= 18  && e.keyCode!= 17  && e.keyCode!= 38  && e.keyCode!= 40  && e.keyCode!= 9  && e.keyCode!= 13  && e.keyCode!= 16)
    {
      checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
    }
  })
  $
}

// This function is called after an onchange event occurs in zip, credit card number, surcharge type, or amount. If the inputs pass validation it sends an ajax call to receieve the transactionFee and updates the Surcharge Value
checkandsend = (creditcardnumber, zip, amount) => {

  if (!!creditcardnumber && !!zip && !!amount) {
    var name = $(".name").text().toLowerCase().replace(' ', '_')
    var company_name = $("//span[text()='Account ID']/../div/text() #merchant-header-company-name").text().toLowerCase().replace(' ', '_')
    var data_array = []
    data_array.push(name)
    data_array.push(company_name)
    var errorexist = false
    var errors = { ccnumber_error: false, zipcode_error: false, surcharge_type_error: false, amount_error: false }
    var onlynumbers_asterisk = new RegExp('^[0-9 *]+$');
    var onlynumbers = new RegExp('^[0-9]+$'); 
    var creditcardnumberstring = creditcardnumber.toString().replace(/\s+/g, '')

    var validatecreditcardnum = false;
    if (creditcardnumberstring.includes('*')) {
      onlynumbersvalidation = onlynumbers_asterisk.test(creditcardnumberstring)
      if (!!onlynumbersvalidation && creditcardnumberstring.length >= 8) {
        nicn = creditcardnumberstring.substring(0,8)
        validatecreditcardnum = true
      } else if (!!!onlynumbersvalidation || creditcardnumberstring.length < 15) {
        console.log("valid cc false", onlynumbersvalidation, creditcardnumberstring.length)
        validatecreditcardnum = false
      } else {
        console.log("valid cc false")
        validatecreditcardnum = false
      }
    } else {
      onlynumbersvalidation = onlynumbers.test(creditcardnumberstring)
      if (!!onlynumbersvalidation && creditcardnumberstring.length >= 8) {
        nicn = creditcardnumberstring.substring(0,8)
        validatecreditcardnum = true
      } else if (!!!onlynumbersvalidation || creditcardnumberstring.length < 8) {
        console.log("valid cc false", onlynumbersvalidation, creditcardnumberstring.length)
        validatecreditcardnum = false
      } else {
        console.log("valid cc false")
        validatecreditcardnum = false
      }
    }

    var validatezip = onlynumbers.test(zip)
    var validateamount = /^\d+\.\d\d$/.test(amount.toString())

    if (validatecreditcardnum == true) {
      errors.ccnumber_error = false
    }
    else if (validatecreditcardnum == false) {
      errors.ccnumber_error = true
    }
    if (validatezip == true && zip.length == 5) {
      errors.zipcode_error = false
    }
    else if (validatezip == true || zip.length != 5) {
      errors.zipcode_error = true
    }
    if (validateamount == true) {
      errors.amount_error = false
    }
    else if (validateamount == false) {
      errors.amount_error = true
    }
    else if (validatezip == false) {
      errors.amount_error = true
    }

    for (var key in errors) {
      if (errors[key] == true) {
        errorexist = true
      }
    }

    if (errorexist == true) {
      console.log(errors)
    } else if (errorexist == false) {
      var p = !!processor ? processor : undefined
      var s = !!sTxId ? sTxId : undefined
      dataz = { amount: amount, country: 840, nicn: nicn, region: zip, sTxId: sTxId, data: data_array, processor: processor }

      var urlzz = endpoint + "/v1/ch"
      // setTimeout(
      $.ajax({
        url: endpoint + "/v1/ch",
        beforeSend: function (request) {
          request.setRequestHeader("Authorization", 'Bearer ' + apikey);
          request.setRequestHeader("X-Requested-With","xhr")
        },
        type: 'POST',
        async: id_set,
        data: dataz,
        success: function (response) {

          if (!response || response.transactionFee == null) {
            console.log("could not parse surchage response", JSON.stringify(response || {}))
            return
          }

          var total = response.transactionFee

          var formattedTotal = parseFloat(total).toFixed(2);
          if (formattedTotal > 0.0) {
            console.log("setting calculated surcharge", JSON.stringify(response || {}))
            surchargeCheck()
            $("#surcharge_display").val(formattedTotal)
            // $("#surcharge_display").focus()
            // $("#zip").focus()
            $("#surcharge_type").val(2)
            changeSurchargeVisible("")
            updateTotal()
          } else {
            console.log("clearing calculated surcharge", JSON.stringify(response || {}))
            $("#surcharge_type").val(1)
            surchargeCheck()
          }

          if (id_set == false) {
            sessionStorage.setItem('stxid',response.sTxId);
            sTxId = response.sTxId;
            id_set = true;
          }
        },
        error:function(error){
          console.log("error calculating surcharge", error)
          console.log(error)
        }
      }) // , 150)
    }

  }
  else
    return

}

