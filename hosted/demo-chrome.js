// This is the script that is pulled from server and injected
// --------HELPER FUNCTIONS--------
// Variables used to verify whether the user is on the transaction success page.
currenturl = window.location.href
console.log(currenturl)
if (currenturl.includes('https://secure.cardflexonline.com/merchants/virtualterminal.php') == true || currenturl.includes('http://localhost') == true)
{
  sales_page_confirmation = true
}

console.log(sales_page_confirmation)
sales_page_transaction_receipt_page_confirmation = $('#ccnumber').length

// If creditcardnumber contains * it means that the user used the sales page and this code declares the variables with its inputs
check_asterisk_in_cc_number = () => {
 let ccnum = $("#ccnumber").val()
 if (typeof ccnum !='undefined')
 {
  if (ccnum.includes('*') == true)
  {
    creditcardnumber = $("#ccnumber").val()
    zip = $("#zip").val()
    amount = $("#amount").val()
    
  }
 } 
 
}
// This code is used to replace the amount input with a new input that does not contain the onblur event which contained a function that would automatically change the total of the surcharge to 0 when the user unfocused it
replace_amount_input = () => {
$("#amount").replaceWith('<input type="text" class="form-control" id="amount" name="amount_hidden" value="" onchange="validate("virtualterminal_ccsale", this);" style="" placeholder="00.00" tabindex="103">');
}

// This code is used to extract the authorization code to be posted to the capture api. 
extract_authcode = () => {
  var parent = $("#cc-details-body").children()
  var children = $(parent[1]).children()
  var authcode = $(children[2]).text().replace(/\D/g, '');
  return authcode
};
clicked_to_set_clicked = () =>{
  zip = $("#zip").val()
  surcharge_type_int = $("#surcharge_type").val()
  creditcardnumber = $("#ccnumber").val()
  amount = $("#amount").val()
  checkandsend(creditcardnumber, surcharge_type_int, zip, amount)

}



// This code is used to extract the transaction id to be posted to the capture api as mTxId 
extract_mTxId = () => {
  var parent = $(".panel-body").children()
  var children1 = parent[2]
  var text = $(children1).text().replace(/\D/g, '');
  return text
}
// Remove saved stxid that is used to be posted to capture api
delete_old_stxid = () =>{
  if (typeof sessionStorage.getItem('stxid') != null && sales_page_confirmation == true && sales_page_transaction_receipt_page_confirmation == 1 )
{
    sessionStorage.removeItem('stxid')
}
}
// ---End Helper Functions---

// This code is used to change the amount when set amount is pressed.

$("#level3total").click(() => {
  setTimeout(clicked_to_set_clicked,450)

})

let dog = $("#level3total").parent()
$(dog).click(() => {
  setTimeout(clicked_to_set_clicked,450)

})
// Declare global variables
// sTxId and id_set is used to keep track of the current sTxId
var sTxId;
var id_set = false;
// set Surcharge Type to Fixed when script is injected
$("#surcharge_type").val(2)
var creditcardnumber;
var surcharge_type_int = 2;
var zip;
var amount;
var setamountpressed = false



// retreive endpoint and apikey from sessionStorage
var endpoint = sessionStorage.getItem('endpoint')
var apikey = sessionStorage.getItem('apikey')
check_asterisk_in_cc_number()
replace_amount_input()
delete_old_stxid()



// detects change in input
if (sales_page_confirmation == true) {
  $("#amount").keyup((e) => {
    amount = e.target.value
    checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
    
  })
  $("#zip").keyup((e) => {
    zip = e.target.value
    checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
  })
  $("#surcharge_type").change((e) => {
    surcharge_type_int = e.target.value
    checkandsend(creditcardnumber, surcharge_type_int, zip, amount)

  })
  $("#ccnumber").keyup((e) => {
    creditcardnumber = e.target.value
    checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
  })
}
// Check if on the transaction page and send call to capture api
if (sales_page_confirmation == true && sales_page_transaction_receipt_page_confirmation == 0 && sessionStorage.getItem('stxid') != null) {
 
        let dataz = { sTxId: sessionStorage.getItem('stxid') , authCode: extract_authcode(), mTxId: extract_mTxId() }
        
          
          $.ajax({
            url: endpoint + "/v1/ch/capture",
            beforeSend: function (request) {
              request.setRequestHeader("Authorization", 'Bearer ' + apikey);
            },
            type: 'POST',
            data: dataz,
            success: function (response) {
              
              sessionStorage.removeItem('stxid')


            }
          })
        
}
// This function is called after an onchange event occurs in zip, credit card number, surcharge type, or amount. If the inputs pass validation it sends an ajax call to receieve the transactionFee and updates the Surcharge Value
checkandsend = (creditcardnumber, surcharge_type_int, zip, amount) => {

  if (typeof creditcardnumber != 'undefined' && typeof surcharge_type_int != 'undefined' && typeof zip != 'undefined' && typeof amount != 'undefined') {
    var name = $(".name").text().toLowerCase().replace(' ', '_')
    var company_name = $("#merchant-header-company-name").text().toLowerCase().replace(' ', '_')
    var data_array = []
    data_array.push(name)
    data_array.push(company_name)
    var processor = $("#processor_id").val()
    var errorexist = false
    if (surcharge_type_int == 1) {
      surcharge_type = 'No Surcharge'
    }
    else if (surcharge_type_int == 2) {
      surcharge_type = 'Fixed'
    }
    else if (surcharge_type_int == 3) {
      surcharge_type = 'Percentage'
    }
    var errors = { ccnumber_error: false, zipcode_error: false, surcharge_type_error: false, amount_error: false }
    var onlynumbers = new RegExp('^[0-9 *]+$');
    var creditcardnumberstring = creditcardnumber.toString()
    if (creditcardnumberstring.includes('*') == true) {
      amount = $("#amount").val()
      zip = $("#zip").val()
      creditcardnumber = $("#ccnumber").val()

      if (creditcardnumber.length >= 12) {
        validatecreditcardnum = true
      }
      else if (creditcardnumber.length < 12) {
        validatecreditcardnum = false
      }
    }
    else {
      var validatecreditcardnum = creditcardnumber.toString().match(/^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)
    }
    var validatezip = onlynumbers.test(zip)
    var validateamount = /^\d+\.\d\d$/.test(amount.toString())

    if (validatecreditcardnum != null) {
      errors.ccnumber_error = false
    }
    else if (validatecreditcardnum == null) {
      errors.ccnumber_error = true
    }
    if (validatezip == true && zip.length == 5) {
      errors.zipcode_error = false
    }
    else if (validatezip == true || zip.length != 5) {
      errors.zipcode_error = true
    }
    if (validateamount == true) {
      errors.amount_error = false
    }
    else if (validateamount == false) {
      errors.amount_error = true
    }
    else if (validatezip == false) {
      errors.amount_error = true
    }
    if (surcharge_type == 'Fixed') {
      errors.surcharge_type_error = false
    }
    else if (surcharge_type != 'Fixed') {
      errors.surcharge_type_error = true
    }
    console.log(errors)
    for (var key in errors) {
      if (errors[key] == true) {
        
        errorexist = true
      }
    }
    if (errorexist == true) {
      
    }
    else if (errorexist == false) {

      let nicnz = creditcardnumber.substring(0, 6);
      if (id_set == false) {

        dataz = { amount: amount, country: 840, nicn: nicnz, region: zip, data: data_array }
        if (processor != '') {
          dataz = { amount: amount, country: 840, nicn: nicnz, region: zip, data: data_array, processor: processor }
        }

      }
      else if (id_set == true) {
        dataz = { amount: amount, country: 840, nicn: nicnz, region: zip, sTxId: sTxId, data: data_array }
        if (processor != '') {
          dataz = { amount: amount, country: 840, nicn: nicnz, region: zip, sTxId: sTxId, data: data_array, processor: processor }
        }
      }
        let urlzz = endpoint + "/v1/ch"
        
          setTimeout($.ajax({
            url: endpoint + "/v1/ch",
            beforeSend: function (request) {
              request.setRequestHeader("Authorization", 'Bearer ' + apikey);
              request.setRequestHeader("X-Requested-With","xhr")
            },
            type: 'POST',
            async: id_set,
            data: dataz,
            success: function (response) {
              
              $("#surcharge_display").val(response.transactionFee)
              let amountz = parseFloat($("#amount").val())
              let total = amountz + response.transactionFee
              console.log(total)
              $("#total").val(total)

              console.log($("#surcharge_display").val())
              console.log(response.transactionFee)
              
              if (id_set == false) {
                sessionStorage.setItem('stxid',response.sTxId);
                sTxId = response.sTxId;
                id_set = true;
              }
              
              


            }
          }), 150)
   
    }

  }
  else
    return

}