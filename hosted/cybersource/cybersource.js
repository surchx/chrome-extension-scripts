currenturl = window.location.href

let currentPage = undefined

const endpoint = sessionStorage.getItem('endpoint')
const token = sessionStorage.getItem('token')

console.log("Initializing CyberSource plugin")
//debugger

const processor = undefined // 'processor'

const POLL_TIME = 1000
const MAX_POLL_COUNT = 10

let pollCount = 0

let amountValue = ''
let cardNumberValue = ''
let regionValue = ''

async function tfCallback(e) {
  if (!e && !e.data) {
    return;
  }
  if (e.data.surchMsgType === "tfCallback") {
    console.log("tfCallback returned successfully!", e.data)
    const transactionFee = (e.data.body || {}).transactionFee
    console.log(`Returned transaction fee is ${transactionFee}`)
    if (transactionFee !== undefined) {
      const { tf } = getInputs()
      tf.value = transactionFee
    }
  }
}
window.addEventListener("message", tfCallback, false);

const pollForHeader = () => {
  pollCount += 1
  const [...headerNodes] = document.querySelectorAll('.css-q9x4sg-all-structMargin-pageTitle-pageTitle').values()
  if (headerNodes.length > 0) {
    if (checkPageMatch(headerNodes)) initInputListeners()
  }
  else {
    if (pollCount < MAX_POLL_COUNT) window.setTimeout(pollForHeader, POLL_TIME)
    else console.log('Max polls reached without finding header.')
  }
}

const checkPageMatch = (headerNodes) => {
  return headerNodes.filter(n => {
    const [...childNodes] = n.childNodes.values()
    return childNodes.filter(c => c.nodeName === 'SPAN' && c.textContent === 'One-Time Payment').length > 0
  }).length > 0
}

const initInputListeners = () => {
  const { amount, cardNumber, region, tf } = getInputs()
  amount.addEventListener('change', (event) => {
    amountValue = event.target.value.toString()
    checkAndSend()
  })
  cardNumber.addEventListener('keyup', (event) => {
    if (!event.target.value.toString().includes('X')) {
      cardNumberValue = event.target.value.toString().replace(/\s+/g, '')
      checkAndSend()
    }
  })
  region.addEventListener('change', (event) => {
    regionValue = event.target.value.toString()
    checkAndSend()
  })
}

const getInputs = () => {
  const amount = document.evaluate("//label[text()='Amount *']/..//input", document, null, XPathResult.ANY_TYPE, null).iterateNext()
  const cardNumber = document.evaluate("//label[text()='Credit Card Number *']/..//input", document, null, XPathResult.ANY_TYPE, null).iterateNext()
  const region = document.evaluate("//label[text()='Zip/Postal Code *']/..//input", document, null, XPathResult.ANY_TYPE, null).iterateNext()
  const tf = document.evaluate("//label[text()='transaction_fee*']/..//input", document, null, XPathResult.ANY_TYPE, null).iterateNext()
  return { amount, cardNumber, region, tf }
}

const checkAndSend = () => {
  console.log('check and send running', Number(amountValue))

  const verbose = amountValue && Number(amountValue) === 101

  if (verbose) {
    console.log('amount value', amountValue)
    console.log('card number value', cardNumberValue)
    console.log('region value', regionValue)
  }

  if (!amountValue || !cardNumberValue || !regionValue) return false

  const onlyNumbers = new RegExp('^[0-9]+$')
  const onlyNumbersAndAsterisk = new RegExp('^[0-9 *]+$')
  const isAmountFormat = /^\d+\.\d\d$/

  const isCardValid = onlyNumbersAndAsterisk.test(cardNumberValue) && cardNumberValue.length === 16
  const isRegionValid = onlyNumbers.test(regionValue) && regionValue.length >= 5
  const isAmountValid = isAmountFormat.test(amountValue)

  if (verbose) {
    console.log('is card valid', isCardValid)
    console.log('is region valid', isRegionValid)
    console.log('is amount valid', isAmountValid)
  }

  if (!isCardValid || !isRegionValid || !isAmountValid) return false

  const nicn = cardNumberValue.substring(0, 8)

  const requestData = {
    amount: amountValue,
    country: 840,
    nicn,
    region: regionValue,
    //sTxId,
    //data: data_array,
    processor
  }

  window.parent.postMessage({
    surchMsgType: "tf",
    body: requestData,
    endpoint,
    token: token,
  })
}

pollForHeader()
