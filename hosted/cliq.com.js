currenturl = window.location.href

//
// send a message back to the main extension that we only want to
// run on the following URIs
//
window.parent.postMessage({
  surchMsgType: "matches",
  matches: [
    "https://secure.cardflexonline.com"
  ]
});

var currentPage = "creditCardSale"

var breacrumb = $("#gateway-breadcrumb").find("li.breadcrumb-item.active")
if (breacrumb) {
  var breadcrumbText = breacrumb.text()
  if (breadcrumbText) {
    if (breadcrumbText === "Virtual Terminal CC Sale") {
      currentPage = "creditCardSale"
    } if (breadcrumbText === "Virtual Terminal CC Auth Only") {
      currentPage = "creditCardAuth"
    //} if (breadcrumbText === "Virtual Terminal CC Auth Only") {
    //  currentPage = "creditCardAuth"
    } else {
      console.log("could not determine currentPage")
    }
  }
}
console.log("BREADCRUMB", breacrumb.text())

if (currenturl.includes('https://secure.cardflexonline.com/merchants/virtualterminal.php') == true)
{
  sales_page_confirmation = true
}
sales_page_transaction_receipt_page_confirmation = $('#ccnumber').length

// This code is used to replace the amount input with a new input that does not contain the onblur
// event which contained a function that would automatically change the total of
// the surcharge to 0 when the user unfocused it
replace_amount_input = () => {
$("#amount").replaceWith('<input type="text" class="form-control" id="amount" name="amount_hidden" value="" onchange="validate(\'virtualterminal_ccsale\', this);" style="" placeholder="00.00" tabindex="103">');
}
// This code is used to extract the authorization code to be posted to the capture api. 
extract_authcode = () => {
  var withWhitespace = $("b:contains('Auth. Code:')").parent().text()
  var s = withWhitespace.split(":")
  var text = undefined
  if (s.length == 2) {
    text = s[1].replace(/\D/g, '')
  } else {
    console.log("could not parse authCode id", withWhitespace)
  }
  return text
};

clicked_to_set_clicked = () =>{
  zip = $("#zip").val()
  surcharge_type_int = $("#surcharge_type").val()
  creditcardnumber = $("#ccnumber").val()
  amount = $("#amount").val()
  checkandsend(creditcardnumber, surcharge_type_int, zip, amount)

}

// This code is used to extract the transaction id to be posted to the capture api as mTxId
extract_mTxId = () => {
  var withWhitespace = $("b:contains('Transaction ID: ')").parent().text()
  var s = withWhitespace.split(":")
  var text = undefined
  if (s.length == 2) {
    text = s[1].replace(/\D/g, '')
  } else {
    console.log("could not parse transaction id", withWhitespace)
  }
  return text
}

// Remove saved stxid that is used to be posted to capture api
delete_old_stxid = () =>{
  if (typeof sessionStorage.getItem('stxid') != null && sales_page_confirmation == true && sales_page_transaction_receipt_page_confirmation == 1 )
{
    sessionStorage.removeItem('stxid')
}
}
// ---End Helper Functions---

// This code is used to change the amount when set amount is pressed.

$("#level3total").click(() => {
  setTimeout(clicked_to_set_clicked,450)

})

let click_to_set_amount = $("#level3total").parent()
$(click_to_set_amount).click(() => {
  setTimeout(clicked_to_set_clicked,450)
})
// Declare global variables
// sTxId and id_set is used to keep track of the current sTxId
var sTxId;
var id_set = false;
// set Surcharge Type to Fixed when script is injected
$("#surcharge_type").val(2)
var creditcardnumber;
var nicn
var surcharge_type_int = 2;
var zip;
var amount;
var setamountpressed = false



// retreive endpoint and apikey from sessionStorage
var endpoint = sessionStorage.getItem('endpoint')
var apikey = sessionStorage.getItem('apikey')


replace_amount_input()
delete_old_stxid()



// detects change in input
if (sales_page_confirmation == true) {
  $("#amount").keyup((e) => {
    amount = e.target.value
    if (e.keyCode != 39 && e.keyCode != 37 && e.keyCode!= 17  && e.keyCode!= 18  && e.keyCode!= 17  && e.keyCode!= 38  && e.keyCode!= 40  && e.keyCode!= 9  && e.keyCode!= 13  && e.keyCode!= 16)
    {
      checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
    }
    
    
  })
  $("#zip").keyup((e) => {
    zip = e.target.value
    if (e.keyCode != 39 && e.keyCode != 37 && e.keyCode!= 17  && e.keyCode!= 18  && e.keyCode!= 17  && e.keyCode!= 38  && e.keyCode!= 40  && e.keyCode!= 9  && e.keyCode!= 13  && e.keyCode!= 16)
    {
      checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
    }
  })
  $("#surcharge_type").change((e) => {
    surcharge_type_int = e.target.value
    
      checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
    
     
    // remove stxid to prevent capture from calling
    if (surcharge_type_int != 2)
    {
      sessionStorage.removeItem('stxid')
      
    }

  })
  $("#ccnumber").keyup((e) => {
    creditcardnumber = e.target.value
    if (e.keyCode != 39 && e.keyCode != 37 && e.keyCode!= 17  && e.keyCode!= 18  && e.keyCode!= 17  && e.keyCode!= 38  && e.keyCode!= 40  && e.keyCode!= 9  && e.keyCode!= 13  && e.keyCode!= 16)
    {
      checkandsend(creditcardnumber, surcharge_type_int, zip, amount)
    }
  })
  $
}
// Check if on the transaction page and send call to capture api
if (sales_page_confirmation == true && sales_page_transaction_receipt_page_confirmation == 0 && sessionStorage.getItem('stxid') != null) {
 
        let dataz = { sTxId: sessionStorage.getItem('stxid') , authCode: extract_authcode(), mTxId: extract_mTxId() }
        
          
          $.ajax({
            url: endpoint + "/v1/ch/capture",
            beforeSend: function (request) {
              request.setRequestHeader("Authorization", 'Bearer ' + apikey);
            },
            type: 'POST',
            data: dataz,
            success: function (response) {
              
              sessionStorage.removeItem('stxid')


            },
            error:function(error){
              console.log(error)
            }
          })
        
}
// This function is called after an onchange event occurs in zip, credit card number, surcharge type, or amount. If the inputs pass validation it sends an ajax call to receieve the transactionFee and updates the Surcharge Value
checkandsend = (creditcardnumber, surcharge_type_int, zip, amount) => {

  if (!!creditcardnumber && !!surcharge_type_int && !!zip && !!amount) {
    var name = $(".name").text().toLowerCase().replace(' ', '_')
    var company_name = $("#merchant-header-company-name").text().toLowerCase().replace(' ', '_')
    var data_array = []
    data_array.push(name)
    data_array.push(company_name)
    var processor = $("#processor_id").val()
    var errorexist = false
    if (surcharge_type_int == 1) {
      surcharge_type = 'No Surcharge'
    }
    else if (surcharge_type_int == 2) {
      surcharge_type = 'Fixed'
    }
    else if (surcharge_type_int == 3) {
      surcharge_type = 'Percentage'
    }
    var errors = { ccnumber_error: false, zipcode_error: false, surcharge_type_error: false, amount_error: false }
    var onlynumbers_asterisk = new RegExp('^[0-9 *]+$');
    var onlynumbers = new RegExp('^[0-9]+$'); 
    var creditcardnumberstring = creditcardnumber.toString()
    var validatecreditcardnum;
    if (creditcardnumberstring.includes('*')) {
      amount = $("#amount").val()
      zip = $("#zip").val()
      creditcardnumber = $("#ccnumber").val()
      onlynumbersvalidation = onlynumbers_asterisk.test(creditcardnumberstring)
      if (!!onlynumbersvalidation && creditcardnumberstring.length >= 8) {
        nicn = creditcardnumberstring.substring(0,8)
        validatecreditcardnum = true
      } else if (!!!onlynumbersvalidation || creditcardnumberstring.length < 15) {
        validatecreditcardnum = false
      }
    } else {
      onlynumbersvalidation = onlynumbers.test(creditcardnumberstring)
      if (onlynumbersvalidation == true && creditcardnumberstring.length >= 15)
      {
        nicn = creditcardnumberstring.substring(0,8)
        validatecreditcardnum = true
      }
        
      else if (onlynumbersvalidation == false || creditcardnumberstring.length < 15)
      {
        validatecreditcardnum = false
      }
    }
    var validatezip = onlynumbers.test(zip)
    var validateamount = /^\d+\.\d\d$/.test(amount.toString())

    if (validatecreditcardnum == true) {
      errors.ccnumber_error = false
    }
    else if (validatecreditcardnum == false) {
      errors.ccnumber_error = true
    }
    if (validatezip == true && zip.length == 5) {
      errors.zipcode_error = false
    }
    else if (validatezip == true || zip.length != 5) {
      errors.zipcode_error = true
    }
    if (validateamount == true) {
      errors.amount_error = false
    }
    else if (validateamount == false) {
      errors.amount_error = true
    }
    else if (validatezip == false) {
      errors.amount_error = true
    }

    for (var key in errors) {
      if (errors[key] == true) {
        errorexist = true
      }
    }

    if (errorexist == true) {
      console.log(errors)
    } else if (errorexist == false) {
      var p = !!processor ? processor : undefined
      var s = !!sTxId ? sTxId : undefined
      dataz = { amount: amount, country: 840, nicn: nicn, region: zip, sTxId: sTxId, data: data_array, processor: processor }

      var urlzz = endpoint + "/v1/ch"
      // setTimeout(
      $.ajax({
        url: endpoint + "/v1/ch",
        beforeSend: function (request) {
          request.setRequestHeader("Authorization", 'Bearer ' + apikey);
          request.setRequestHeader("X-Requested-With","xhr")
        },
        type: 'POST',
        async: id_set,
        data: dataz,
        success: function (response) {

          if (!response || response.transactionFee == null) {
            console.log("could not parse surchage response", JSON.stringify(response || {}))
            return
          }

          var total = response.transactionFee

          var formattedTotal = parseFloat(total).toFixed(2);
          if (formattedTotal > 0.0) {
            console.log("setting calculated surcharge", JSON.stringify(response || {}))
            surchargeCheck()
            $("#surcharge_display").val(formattedTotal)
            // $("#surcharge_display").focus()
            // $("#zip").focus()
            $("#surcharge_type").val(2)
            changeSurchargeVisible("")
            updateTotal()
          } else {
            console.log("clearing calculated surcharge", JSON.stringify(response || {}))
            $("#surcharge_type").val(1)
            surchargeCheck()
          }

          if (id_set == false) {
            sessionStorage.setItem('stxid',response.sTxId);
            sTxId = response.sTxId;
            id_set = true;
          }
        },
        error:function(error){
          console.log("error calculating surcharge", error)
          console.log(error)
        }
      }) // , 150)
    }

  }
  else
    return

}

