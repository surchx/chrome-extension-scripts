
currenturl = window.parent.location.href
console.log("validating URI ", currenturl);

var sales_page_confirmation = true

console.log('posting message');
var m = {
  surchMsgType: "matches",
  matches: [
    ".*"
  ]
};

window.parent.postMessage(m);

function doit($) {
  console.log("running jquery", $.fn.jquery);
}

(function() {

  if (window.jQuery) {
    console.log("jquery already installed");
  } else {
    var startingTime = new Date().getTime();
    // Load the script
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existance
    var checkReady = function(callback) {
      if (window.jQuery) {
        callback(jQuery);
      }
      else {
        window.setTimeout(function() { checkReady(callback); }, 20);
      }
    };

    //$.noConflict();
    //jQuery(document).ready(function($) {})

    // Start polling...
    checkReady(function(jQuery) {
      jQuery(function() {
        var endingTime = new Date().getTime();
        var tookTime = endingTime - startingTime;
        doit(jQuery);
      });
    });
  }

})();

